# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'whatsapp_db_merge/version'

Gem::Specification.new do |spec|
  spec.name          = "whatsapp_db_merge"
  spec.version       = WhatsappDbMerge::VERSION
  spec.authors       = ["disterics"]
  spec.email         = ["disterics@wojeshun.net"]
  spec.description   = %q{Tool to merge multiple whatsapp databases}
  spec.summary       = %q{Merge multiple whatsapp databases}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency 'activerecord'
  spec.add_runtime_dependency 'sqlite3'
  spec.add_runtime_dependency 'thor',     '~> 0.18.1'

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
