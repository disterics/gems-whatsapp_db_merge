module WhatsappDbMerge
  require 'active_record'

  class Message < ActiveRecord::Base
    after_save :update_chat_list

    private

    def update_chat_list
      unless self.key_remote_jid == "-1"
        puts "#{self.key_remote_jid} -> #{self._id}"
        chat = Chat.find_by_key_remote_jid(self.key_remote_jid)
        raise "Chat not found" if chat.nil?
        chat.update(message_table_id: self._id)
        chat.save
      end
    end

  end
end
