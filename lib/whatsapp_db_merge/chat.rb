module WhatsappDbMerge
  require 'active_record'

  class Chat < ActiveRecord::Base
    self.table_name = "chat_list"
    has_one :message

  end

end
