require 'thor'

require 'whatsapp_db_merge'
require 'whatsapp_db_merge/db_merger'
require 'whatsapp_db_merge/version'

module WhatsappDbMerge
  class CLI < Thor

    default_task :merge

    option :output, :required => true
    desc "merge DATABASES -o DATABASE", "Merge the whatsapp databases"
    def merge(*databases)
      merger = DbMerger.new(databases, options[:output])
      merger.merge
    end

  end

end
