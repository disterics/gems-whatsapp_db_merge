module WhatsappDbMerge
  class CreateDb < ActiveRecord::Migration
    def up
      create_table :chat_list do |c|
        c.primary_key :_id,
        c.text    :key_remote_jid,
        c.integer :message_table_id
        c.text    :subject
        c.integer :creation
      end

      create_table :messages do |m|
        m.primary_key :_id
        m.text :key_remote_jid, null: false
        m.integer :key_from_me
        m.text :key_id, null: false
        m.integer :status
        m.integer :needs_push
        m.text :data
        m.integer :timestamp
        m.text :media_url
        m.text :media_mime_type
        m.text :media_wa_type
        m.integer :media_size
        m.text :media_name
        m.text :media_hash
        m.integer :media_duration
        m.integer :origin
        m.real :latitude
        m.real :longitude
        m.text :thumb_image
        m.text :remote_resource
        m.integer :received_timestamp
        m.integer :send_timestamp
        m.integer :receipt_server_timestamp
        m.integer :receipt_device_timestamp
        m.blob :raw_data
        m.integer :recipient_count
      end

    end

    def down
    end
  end
end
