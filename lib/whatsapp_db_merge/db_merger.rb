module WhatsappDbMerge

  require 'whatsapp_db_merge/database'

  class DbMerger

    include Thor::Shell

    def initialize(databases, output)
      @databases = databases.collect { |db| Database.new(db, readonly: true) }
      @output = Database.new(output)
      @output.schema
    end

    def merge
      chats = get_chats
      messages = get_messages
      _open_output
      merge_chat_list(chats)
      merge_messages(messages)
    end

    private

    def merge_chat_list(chats)
      chats.map { |c| @output.create_or_update_chat(c) }
    end

    def merge_messages(messages)
      init = messages.first.dup
      init.save
      messages.map { |m| @output.insert_message(m) }
      # m1 = messages.first
      # say m1.inspect
      # mn = m1.dup
      # say mn.inspect
      # mn.save
      # say mn.inspect
    end

    def get_messages
      messages = @databases.inject(Array.new) { |memo, db| db.messages + memo }
      messages.sort_by { |m| m.timestamp }
    end

    def get_chats
      chats = @databases.inject(Array.new) { |memo, db| db.chats + memo }
      chats.sort_by { |m| m._id }
    end

    def _open_output
      ActiveRecord::Base.clear_active_connections!
      @output.connect
    end

  end
end
