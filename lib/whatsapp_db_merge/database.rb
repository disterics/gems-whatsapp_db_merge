module WhatsappDbMerge

  require 'active_record'
  require 'whatsapp_db_merge/chat'
  require 'whatsapp_db_merge/message'

  class Database

    include Thor::Shell

    def initialize(database, options = {})
      @database = database
      @options = options
      @connected = false
      say "Database: #{database}"
    end

    def messages
      connect
      Message.all
    end

    def chats
      connect
      Chat.all
    end

    def schema
      connect
      c = Chat.new
      m = Message.new
    end

    def connect
      unless @connected
        ActiveRecord::Base.establish_connection(adapter: 'sqlite3', database: @database, readonly: true)
        connected = true
      end
    end

    def create_or_update_chat(chat)
      Chat.find_or_create_by(key_remote_jid: chat.key_remote_jid) do | c |
        attr = chat.attributes
        new_attr = {}
        creation = attr["creation"]
        subject = attr["subject"]
        unless creation.nil?
          unless c.creation.nil?
            subject = c.subject if c.creation < creation
            creation = c.creation if c.creation < creation
          end
        end
        c.update(subject: subject, creation: creation)
      end
    end

    def insert_message(message)
      # say message.inspect
      unless message.key_remote_jid == "-1"
        n = message.dup
        n.save
      end
    end

  end
end
